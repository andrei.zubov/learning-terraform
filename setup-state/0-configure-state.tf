provider "google" {
  project = "azubov-a74d3d88"
  region  = "us-central1"
  zone    = "us-central1-c"
}

terraform {

}

resource "google_storage_bucket" "tf-state" {
  name = "azubov-tf-state-bucket"
  location = "US" # that's a multi-region location
}
