variable "region" {
  type    = string
  default = "us-central1"
}

variable "zone" {
  type    = string
  default = "us-central1-c"
}

variable "project" {
  type    = string
  default = "azubov-a74d3d88"
}

variable "node_count" {
  type    = number
  default = 2
}
