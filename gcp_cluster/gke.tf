resource "google_container_cluster" "primary" {
  name     = "${var.project}-gke"
  location = var.region

  # next two lines are here to have the default node pool removed
  remove_default_node_pool = true
  initial_node_count       = 1

  network    = google_compute_network.vpc.self_link
  subnetwork = google_compute_subnetwork.subnet.self_link
}

resource "google_container_node_pool" "primary_nodes" {
  name       = "${var.project}-primary-nodes"
  location   = var.region
  cluster    = google_container_cluster.primary.self_link
  node_count = 2

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      "env" = var.project
    }

    machine_type = "n1-standard-1"
    tags         = ["gke-node", "${var.project}-gke"]

    metadata = {
      "disable-lefacy-endpoints" = "true"
    }
  }
}
