terraform {
  backend "gcs" {
    bucket = "azubov-tf-state-bucket"
    prefix = "terraform/state"

  }
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.0"
    }
  }
}

provider "google" {
  project = var.project
  region  = var.region
  zone    = var.zone
}
