terraform {
}

provider "google" {
  project = "azubov-a74d3d88"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_compute_instance" "vm_instance" {
  name         = "terraform-instance"
  machine_type = "e2-micro"

  metadata_startup_script = <<SCRIPT
    #!/bin/bash
    sudo apt-get update
    sudo apt-get install apache2 -y
    echo '<!doctype html><html><body><h1>Hello World!</h1></body></html>' | sudo tee /var/www/html/index.html
  SCRIPT

  tags = [ "web" ]

  boot_disk {
    initialize_params {
        image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    # this is a terraform expression: <PROVIDER>_<TYPE>.<NAME>.<ATTRIBUTE>
    network = google_compute_network.vpc_network.self_link
    access_config {

    }
  }
}

resource "google_compute_firewall" "firewall" {
  name = "terraform-firewall"
  network = google_compute_network.vpc_network.self_link

  allow {
    protocol = "tcp"
    ports = ["80", "22"]
  }

  target_tags = [ "web" ]
  source_ranges = [ "0.0.0.0/0" ]
}

resource "google_compute_network" "vpc_network" {
  name = "terraform-network"
  auto_create_subnetworks = true
}

output "instance_ips" {
  value = "${join(" ", google_compute_instance.vm_instance.*.network_interface.0.access_config.0.nat_ip)}"
  description = "The public IP address of the newly created instance"
}
